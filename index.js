/* db.fruits.insertMany([
    {
        name: "Apple",
        supplier: "Red Farms Inc.",
        stocks: 20,
        price: 40,
        onSale: true,
    },

    {
        name: "Banana",
        supplier: "Yellow Farms",
        stocks: 15,
        price: 20,
        onSale: true,
    },

    {
        name: "Kiwi",
        supplier: "Green Farming and Canning",
        stocks: 25,
        price: 50,
        onSale: true,
    },

    {
        name: "Mango",
        supplier: "Yellow Farms",
        stocks: 10,
        price: 60,
        onSale: true,
    },
    {
        name: "Dragon Fruit",
        supplier: "Red Farms Inc.",
        stocks: 10,
        price: 60,
        onSale: true,
    },
]);
 */

// Aggregation
// AGgregation in MongoDB is typically done 2-3 steps. Each Process/steps in agregation is called stage

db.fruits.aggregate([
    //Aggregate documents to get the total stocks of fruits per supplier

    //$match - used to match or get documents that satisfies the condition
    // Syntax -> {$match: {field:<Value>}}
    { $match: { onSale: true } }, //apple,dragon fruit, banana, mango,kiwi

    // $group - allows us to group together documents and create an analysis out of the grouped documents
    // _id:: in the group stage, essentially associates an id to our results
    // _id: also determins the number of groups

    //_id: "$supplier" - essentially grouped together documents with the same values in the supplier field
    //_id: "$<field>" - groups documents based on the value of the indicate field
    /*
        $match - apple, kiwi, banana, dragon fruit, mango fruit

        $group - _id: $supplier

        $sum - used to add or total values in a given field

        $sum: "$stocks"- we got the sum of the values of stocks field of each item per group

        $sum: "<field>" - sum the values of the given field per group

        _id: Red Farms
        apple
        supllier - Red Farms
        stocks: 20
        
        dragon fruit
        supplier - Red Farms
        stocks: 10

        $sum: 25
        
        _id: Green Farming
        kiwi
        supplier - Green Farming
        stocks: 25
        $sum: 25
        
        _id: Yellow Farms
        banana
        supplier - Yellow Farms
        stocks: 15

        mango
        supplier - Yellow Farms
        stocks: 10$sum: 25



    */
    { $group: { _id: "$supplier", totalStocks: { $sum: "$stocks" } } },
]);
// IF the id's value is definite or given, $group will only create one group
db.fruits.aggregate([
    { $match: { onSale: true } },
    { $group: { _id: "AllFruits", totalStocks: { $sum: "$stocks" } } },
]);

db.fruits.aggregate([
    //$match stage is similar to find().
    //In fact, you can even add query operators to expand the criteria
    { $match: { $and: [{ onSale: true }, { price: { $gte: 40 } }] } },
    { $group: { _id: "$supplier", totalStocks: { $sum: "$stocks" } } },
]);

// $avg - is an operator used in $group stage
// $avg - gets the avg of the values of the given field per group

// gets the average stock of fruites per supplier for all item on sale.
db.fruits.aggregate([
    { $match: { onSale: true } },
    { $group: { _id: "$supplier", avgStock: { $avg: "$stocks" } } },
]);

// gets the average price of all fruits which supplier is Red Farm Inc
db.fruits.aggregate([
    // Apple, Dragon Fruit
    { $match: { supplier: "Red Farms Inc." } },
    { $group: { _id: "group1", avgPrice: { $avg: "$price" } } },
]);

// $max - will allow us to get the highest value out of all the values a given field per group

// gets the highest number of stock of all fruits on sale
db.fruits.aggregate([
    { $match: { onSale: true } },
    { $group: { _id: "maxStockOnSale", maxStock: { $max: "$stocks" } } },
]);

// $max - will allow us to get the highest value out of all the values a given field per group

// gets the highest number of stock of all fruits on sale
db.fruits.aggregate([
    { $match: { onSale: true } },
    { $group: { _id: "maxStockOnSale", maxStock: { $max: "$stocks" } } },
]);

//Gets the highest price of fruits that are on sale
db.fruits.aggregate([
    { $match: { onSale: true } },
    { $group: { _id: "highestPrice", maxPrice: { $max: "$price" } } },
]);

// $min - gets the lowest value of the given field per group
// gets the lowest number of stock of all items on sale
db.fruits.aggregate([
    { $match: { onSale: true } },
    { $group: { _id: "lowestStockOnSale", minStock: { $min: "$stocks" } } },
]);

db.fruits.aggregate([
    { $match: { supplier: "Yellow Farms" } },
    { $group: { _id: "lowestPriceYellowFarms", minPrice: { $min: "$price" } } },
]);

// $count - is a stage usually added after $match to count all items that matches the criteria
// Count all items onSale

db.fruits.aggregate([
    //Banana, Apple, , Kiwi, Mango
    { $match: { onSale: true } },
    { $count: "itemsOnsale" },
]);

db.fruits.aggregate([
    { $match: { price: { $gt: 50 } } },
    { $count: "itemsPriceMoreThan50" },
]);

db.fruits.aggregate([
    { $match: { $and: [{ price: { $gte: 50 } }, { stocks: { $lt: 20 } }] } },
    { $count: "itemsPriceGreaterThan50below20Stocks" },
]);

// $out - save/outputs the results in a new collection.
// it is usually the last stage in an aggregation pipeline
// Note: this will overwrite the collection if it already exists
// the value given for $out becomes the name of the collection to save the results
db.fruits.aggregate([
    { $match: { price: { $lte: 50 } } },
    { $group: { _id: "$supplier", totalStocks: { $sum: "$stocks" } } },
    { $out: "stocksPerSupplier" },
]);
db.fruits.aggregate([
    { $match: { onSale: true } },
    { $group: { _id: "$supplier", avgPrice: { $avg: "$price" } } },
    { $out: "avgPricePerSupplier" },
]);

db.fruits.aggregate([
    { $match: { onSale: true } },
    { $group: { _id: "$supplier", stocks: { $min: "$stocks" } } },
    { $out: "lowestNumberOfStock" },
]);
